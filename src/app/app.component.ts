import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

    constructor(private _service: ApiService){

    }

    ngOnInit(){
      this._service.openSpinner();

      setTimeout(
        () => {
          this._service.closeSpinner();
        }, 2000
      );
    }
}
