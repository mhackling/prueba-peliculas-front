import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from './auth';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable()
export class ApiService {
  private url: any;
  private httpOptions: any;
  private token: any;
  constructor(
    private _http: HttpClient,
    private _spinner: NgxSpinnerService,
    private _auth: AuthService,
    private _router: Router
  ) {
    this.getHeaders();
  }

  getHeaders() {
    this.token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token,
      }),
    };
  }

  getHeadersNoAuth() {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };
  }

  public get(ruta: string) {
    if (this._auth.isAuthenticated()) {
        this.getHeaders();
        return this._http.get<any>(ruta, this.httpOptions);
    }else{
      this.clearSession();
    }
  }

  post(ruta: string, body: any) {
    return this._http.post<any>(ruta, body);
  }

  postAuth(ruta: string, body: any) {
    if (this._auth.isAuthenticated()) {
      this.getHeaders();
      return this._http.post<any>(ruta, body, this.httpOptions);
    }
      this.clearSession();

  }


  put(ruta: string, body: any) {
    if (this._auth.isAuthenticated()) {
      this.getHeaders();
      return this._http.put<any>(ruta, body, this.httpOptions);
    }
      this.clearSession();

  }

  delete(ruta: string) {
    if (this._auth.isAuthenticated()) {
      this.getHeaders();
      return this._http.delete<any>(ruta, this.httpOptions);
    }
      this.clearSession();

  }

  clearSession() {
    this._auth.logOut();
    this._router.navigate(['/login']);
    Swal.fire('¡Aviso!', 'Su sesión ha expirado', 'warning');
  }

  openSpinner() {
    this._spinner.show();
  }

  closeSpinner() {
    this._spinner.hide();
  }
}
