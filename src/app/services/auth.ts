import { HttpClient, HttpHeaders, HttpEvent} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import Swal from 'sweetalert2';

@Injectable()
export class AuthService{
    constructor(private _http: HttpClient, private _router: Router){

    }

    public login(ruta : string, body : any){
      return this._http.post(ruta, body);
    }

    public register(ruta : string, body : any){
      return this._http.post(ruta, body);
    }

    public logOut(): void{
      sessionStorage.clear();
      this._router.navigate(['/login']);
    }

    isAuthenticated():boolean{
      let access_token = sessionStorage.getItem('token');
      const payload = this.obtenerDatosToken(access_token)
      if(payload?.id !=null){
        return true;
      }else{
        return false;
      }
    }

    obtenerDatosToken(access_token: any) {
      if (access_token != null) {
        return JSON.parse(atob(access_token.split('.')[1]))
      }
      return null
    }


}
