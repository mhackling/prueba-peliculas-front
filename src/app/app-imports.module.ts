import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from "ngx-spinner";
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
  declarations: [

  ],
  imports: [
    MatIconModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    CommonModule,
    MatTooltipModule,
    HttpClientModule,
    NgxSpinnerModule,
  ],
  exports: [
    MatIconModule,
    CommonModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTooltipModule,
    HttpClientModule,
    NgxSpinnerModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class AppImports { }
