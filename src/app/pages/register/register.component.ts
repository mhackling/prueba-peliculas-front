import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth';
import { ApiService } from 'src/app/services/service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public formulario : FormGroup;
  constructor(private _router: Router, private _formbuilder: FormBuilder, private _auth: AuthService, private _service: ApiService) {
    this.formulario = this._formbuilder.group({
      'name': ['', Validators.required],
      'username': ['', Validators.required],
      'email': ['', Validators.required],
      'password': ['', Validators.required],
    });
   }

  ngOnInit(): void {
  }

  register(){
    if(this.formulario.valid){
        this._service.openSpinner();
        this._auth.register(`http://localhost:3000/user/register`, this.setForm()).subscribe(
          (response: any) =>{
            Swal.fire('¡Exito!','Registro exitoso','success');
            this._router.navigate(['/login'])
            console.log(this.setForm())
            this._service.closeSpinner();
          },
          (error : any) => {
            Swal.fire('¡Error!','No ha sido posible registrarse','error');
            this._service.closeSpinner();
          }
        );
    }else{
      Swal.fire('¡Aviso!','Ingrese las credenciales de registro correctamente','warning')
    }

  }

  private setForm(){
    let datos = this.formulario.value;

    const formulario = {
      "nombre": datos.name,
      "correo": datos.email,
      "password": datos.password,
      "usuarioscol": datos.username
    }
    return formulario;
  }
}
