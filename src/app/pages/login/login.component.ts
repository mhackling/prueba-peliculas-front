import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth';
import { ApiService } from 'src/app/services/service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public formulario : FormGroup;
  constructor(private _router: Router, private _formBuilder: FormBuilder, private _auth: AuthService,
    private _service: ApiService) {

    this.formulario = this._formBuilder.group({
      'correo': ['', Validators.required],
      'password': ['', Validators.required]
    });

   }

  ngOnInit(): void {
  }

  login(){
    if(this.formulario.valid){
      this._service.openSpinner();
      this._auth.login(`http://localhost:3000/user/login`, this.setForm()).subscribe(
        (response : any) => {
          if(response.status == true){
            sessionStorage.setItem('nameuser', response.nameuser);
            sessionStorage.setItem('token', response.token);
            this._router.navigate(['/dashboard']);
            this._service.closeSpinner();
            Swal.fire('¡Exito!', response.mensaje, 'success')
          }else{
            this._service.closeSpinner();
            Swal.fire('¡Error!', response.error, 'error')
          }

        },
        (error : any) => {
          console.log(error)
          this._service.closeSpinner();
          Swal.fire('¡Error!', 'Login inválido', 'error')
        }
      );

    }else{
      Swal.fire('¡Aviso!', 'Ingrese sus credenciales correctamente', 'warning')
    }

  }

  private setForm(){
    let datos = this.formulario.value;

    const formulario = {
      'correo': datos.correo,
      'password': datos.password
    }

    return formulario
  }

}
