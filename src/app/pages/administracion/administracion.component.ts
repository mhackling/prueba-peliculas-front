import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SelectionModel } from "@angular/cdk/collections";
import { ApiService } from 'src/app/services/service';
import Swal from 'sweetalert2';
import { PeliculasModalComponent } from '../peliculasModal/peliculasModal.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['./administracion.component.scss']
})
export class AdministracionComponent implements OnInit {
  public peliculas : any;
  public datos : FormGroup;
  public fechaInicio : any;
  public fechaFin : any;
  public idioma : any;
  public clasif : any;
  public selection = new SelectionModel<any>(true, []);
  public itemSelected: any;
  public selected = false;
  public estadoPelicula : boolean;
  public clasificacion : Array<any> = [
    {nombre: 'Niños', value: 'AA'},
    {nombre: 'Todo publico', value: 'A'},
    {nombre: '+12', value: 'B'},
    {nombre: 'No apto para menores de 15', value: 'B15'},
    {nombre: '+18', value: 'C'},
    {nombre: 'Adultos mayores', value: 'D'},
  ]

  private swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-primary',
      cancelButton: 'btn btn-danger',
      title: 'title1'
    },
    buttonsStyling: true,
  });

  private setAlerta: any = {
    title: '¿Estás seguro de eliminar la película seleccionada?',
    text: 'Una vez eliminada no se puede recuperar',
    showCancelButton: true,
    confirmButtonText: 'Si',
    cancelButtonText: 'No, cancelar',
    reverseButtons: true,
  };


  private Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });
  constructor(private _service: ApiService, public _dialog: MatDialog, private _formBuilder: FormBuilder) {

    this.datos = this._formBuilder.group({
      clasificacion: ['', Validators.required],
      idioma: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFin: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.getPeliculas();
  }

  public getPeliculas(){
      this._service.openSpinner();
      this._service.get(`http://localhost:3000/peliculas/listPeliculasAdmin`)?.subscribe(
      (response:any) => {
        this.peliculas = response;
        this._service.closeSpinner();
        this.Toast.fire('¡Exito!', 'Listado de peliculas', 'success');
        console.log(this.peliculas);
      },
      error => {
        console.log(error)
        this._service.closeSpinner();
        this.Toast.fire('¡Error!', 'Con la consulta', 'error');
      }
    );
  }

  public setCheckBox(e: any, item: any) {
    if (e.checked === true) {
      this.itemSelected = item;
      this.selection.clear();
      this.selection.select(item);
      this.selected = true;
    } else {
      this.itemSelected = null;
      this.selected = false;
      this.selection.clear();
    }
  }


  public nuevaPelicula(){
    if(!this.selected){
      this.itemSelected = null;
      this.openForm(0)
    }
  }


  public editarPelicula(){
    if(this.selected){
      this.openForm(1)
    }
  }

  public openForm(tipoForm : number){
    const dialogRef = this._dialog.open(PeliculasModalComponent, {
      width: '140vh',
      height: '80vh',
      disableClose : false,
      data: {tipoForm: tipoForm, data: this.itemSelected}
    });
    dialogRef.afterClosed().subscribe(result=>{
        if(result==0){
          this.getPeliculas();
          this.selected=false;
        }
    })
  }

  public eliminar(item:any){
    this.swalWithBootstrapButtons.fire(this.setAlerta).then(
      (res:any)=>{
        if(res.value){
          this._service.delete(`http://localhost:3000/peliculas/deletePelicula/${item.idMovie}`)?.subscribe(
            (res: any)=>{
              Swal.fire('¡Exito!', res.mensaje, 'success')
              this.getPeliculas();
            },
            (err: any)=>{
              Swal.fire('¡Error!', 'Al eliminar', 'error')
            }

          )
        }
      })

  }

  public limpiar(){
    this.fechaInicio = ''
    this.fechaFin = ''
    this.idioma = ''
    this.clasif = ''
    this.getPeliculas()
  }

  public async filtrar(){
    if(!this.datos.valid){
      Swal.fire('¡Aviso!', '¡Debes llenar todos los datos!', 'warning')
    }else{
      console.log(this.fechaInicio)
      console.log(this.fechaFin)
      console.log(this.idioma)
      console.log(this.clasif)
        await this._service.get(
          `http://localhost:3000/peliculas/getPeliculasFilter/${this.clasif}/${this.idioma}/${this.fechaInicio}/${this.fechaFin}`)?.subscribe(
            (res:any)=>{
                this.peliculas = res.result
                Swal.fire('¡Exito!', res.mensaje, 'success')
            },(err:any)=>{
              Swal.fire('¡Error!', 'Al filtrar', 'error')
            }
          )
    }
  }

  async toggleEvent(item : any){

      const itemCambiar = item;
      console.log(itemCambiar)
      await this._service.put(`http://localhost:3000/peliculas/cambiarEstado`, itemCambiar)?.subscribe(
        (res: any) => {
          Swal.fire('¡Exito!', res.mensaje, 'success')
      }, (err: any)=>{
        Swal.fire('¡Error!', 'Al cambiar estado', 'error')
      })
  }
}
