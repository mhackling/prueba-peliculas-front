import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public blogs : Array<any> = [];
  public peliculas : any;
  private Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });
  constructor(private _service: ApiService, public _dialog: MatDialog) { }

  ngOnInit(): void {
    this.getPeliculas();
  }


  public getPeliculas(){
    this._service.openSpinner();
    this._service.get(`http://localhost:3000/peliculas/listPeliculas`)?.subscribe(
    (response:any) => {
      this.peliculas = response;
      this._service.closeSpinner();
      this.Toast.fire('¡Exito!', 'Listado de peliculas', 'success');
      console.log(this.peliculas);
    },
    error => {
      console.log(error)
      this._service.closeSpinner();
      this.Toast.fire('¡Error!', 'Con la consulta', 'error');
    }
  );
}

}
