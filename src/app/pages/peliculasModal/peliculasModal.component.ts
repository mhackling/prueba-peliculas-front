import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/service';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-peliculasModal',
  templateUrl: './peliculasModal.component.html',
  styleUrls: ['./peliculasModal.component.scss']
})
export class PeliculasModalComponent implements OnInit {
  public datos : FormGroup;
  public titulo : any = 'Nuevo'
  public archivos : Array<any> = []
  public previsualizacion : string;
  public poster : any;
  public clasificacion : Array<any> = [
    {nombre: 'Niños', value: 'AA'},
    {nombre: 'Todo publico', value: 'A'},
    {nombre: '+12', value: 'B'},
    {nombre: 'No apto para menores de 15', value: 'B15'},
    {nombre: '+18', value: 'C'},
    {nombre: 'Adultos mayores', value: 'D'},
  ]

  constructor(private _service: ApiService,
    private _sanitizer: DomSanitizer,
    public _datepipe: DatePipe,
    public _dialogRef: MatDialogRef<PeliculasModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _formBuilder: FormBuilder) {
    console.log(data)
    this.datos = this._formBuilder.group({
      nombre: ['', Validators.required],
      idioma: ['', Validators.required],
      clasificacion: ['', Validators.required],
      duracion: ['', Validators.required],
      fecha: ['', Validators.required],
      url: ['', Validators.required],
      sinopsis: ['', Validators.required],
      director: ['', Validators.required],
      reparto: ['', Validators.required],
      poster: ['', ],
      estado: [true, Validators.required],
    })
  }

  ngOnInit(): void {


    if(this.data.tipoForm == 0){
      this.titulo = 'Nueva';
    }else{
      this.titulo = 'Editar';
      this.datos.patchValue({
        ...this.data.data
      });

    }

  }

  public subirArchivo(){
    const formData = new FormData();
    this.archivos.forEach(archivo => {
      formData.append('image', archivo);
      console.log(formData)
    })

    this._service.post('http://localhost:3000/peliculas/subirArchivo', formData).subscribe(
      (response:any)=>{
        Swal.fire('¡Exito!', 'Archivo subido', 'success')
      }, (error)=>{
          console.log(error)
      })
  }

  private setForm(){
    let name;

    this.archivos.forEach(archivo => {
      name = archivo.name
    })


    const formulario = {
      ...this.datos.value,
      poster: name
    }

    return formulario;
  }


  private setFormEditar(){
    let name;

    this.archivos.forEach(archivo => {
      name = archivo.name
    })


    const formulario = {
      ...this.datos.value,
      poster: name,
      fecha: this._datepipe.transform(this.datos.value.fecha, 'yyyy-MM-dd'),
      idMovie: this.data.data.idMovie
    }

    return formulario;
  }

  public async guardar(){
    if(!this.datos.valid){
        Swal.fire('Aviso!', 'Completa todos los datos', 'warning')
    }else{
      this._service.openSpinner();
      console.log(this.setForm())
      await this.subirArchivo();
      this._service.postAuth('http://localhost:3000/peliculas/nuevaPelicula', this.setForm())?.subscribe(
            (response:any)=>{
              this._service.closeSpinner();
              Swal.fire('¡Exito!', 'Pelicula creada', 'success')
              this.close(0)
          }, (error)=>{
                console.log(error)
                this._service.closeSpinner();
                Swal.fire('¡Error!', 'Al guardar', 'error')
          }
          )


    }


  }

  public async editar(){
    if(!this.datos.valid){
        Swal.fire('Aviso!', 'Completa todos los datos', 'warning')
    }else{

      this._service.openSpinner();
      try{
        console.log(this.setFormEditar())
           await this.subirArchivo();
          this._service.put('http://localhost:3000/peliculas/editarPelicula', this.setFormEditar())?.subscribe(
            (response:any)=>{
              this._service.closeSpinner();
              Swal.fire('¡Exito!', response.mensaje, 'success')
              this.close(0)
          }, (error)=>{
                console.log(error)
                this._service.closeSpinner();
                Swal.fire('¡Error!', 'Al editar', 'error')
          }
          )
      }catch(error){
        console.log(error)
      }

    }


  }
  extraerBase64 = async($event: any) => new Promise((resolve, reject) => {
    try{
        const unsafeImg = window.URL.createObjectURL($event);
        const image = this._sanitizer.bypassSecurityTrustUrl(unsafeImg);
        const reader = new FileReader();
        reader.readAsDataURL($event);
        reader.onload = () =>{
          resolve({
            base: reader.result
          });
        }
        reader.onerror = error =>{
          resolve({
            base: null
          })

        }
    }catch(e){
        return null;
    }
  })

  public close(cerrar : number): void {
    this._dialogRef.close(0);
  }

  public capturarImage(event : any){
    const imagenCapturada = event.target.files[0]
    this.extraerBase64(imagenCapturada).then((imagen : any) =>{
      this.previsualizacion = imagen.base;
    })
    this.archivos.push(imagenCapturada)
  }
}
