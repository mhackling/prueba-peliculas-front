import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth';
import { ApiService } from 'src/app/services/service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public username: any;
  public routesNav : Array<any> = [
    {nombre: 'Películas', path: '/dashboard', icon: 'home'},
    {nombre: 'Administración', path: '/admin', icon: 'admin_panel_settings'},
  ]
  constructor(private _auth: AuthService, private _service: ApiService) { }

  ngOnInit(): void {
    this.getDatosUser();
  }

  public logout(){
    this._service.openSpinner();
    this._auth.logOut();
    this._service.closeSpinner();
    Swal.fire('¡Exito!','Cerraste la sesión','success')
  }

  public getDatosUser(){
    this.username = sessionStorage.getItem("nameuser")
  }
}
