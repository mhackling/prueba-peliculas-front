import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from 'src/app/services/service';
import { AuthService } from 'src/app/services/auth';
import { PublicLayoutRoutes } from './public-layout.routing';
import { ComponentsModule } from 'src/app/components/components.module';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AppImports } from 'src/app/app-imports.module';
import { PeliculasModalComponent } from 'src/app/pages/peliculasModal/peliculasModal.component';
import { AdministracionComponent } from 'src/app/pages/administracion/administracion.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule.forChild(PublicLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppImports
  ],
  providers: [AuthService, ApiService, DatePipe],
  declarations: [
    DashboardComponent,
    PeliculasModalComponent,
    AdministracionComponent
  ],
  entryComponents: [
    PeliculasModalComponent
  ],
})

export class PublicLayoutModule {}
