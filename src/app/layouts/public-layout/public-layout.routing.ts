import { Routes } from '@angular/router';
import { AdministracionComponent } from 'src/app/pages/administracion/administracion.component';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';

export const PublicLayoutRoutes: Routes = [
    {path: 'dashboard', component: DashboardComponent},
    {path: 'admin', component: AdministracionComponent}
];
